   using System.IO;
   using Microsoft.AspNetCore.Builder;
   using Microsoft.AspNetCore.Hosting;
   using Microsoft.Extensions.Configuration;
   using Microsoft.Extensions.DependencyInjection;
   using Microsoft.Extensions.Hosting;
   using Ocelot.DependencyInjection;
   using Ocelot.Middleware;

   namespace OcelotBasic
   {
       public class Program
       {
           public static void Main(string[] args)
           {
               new WebHostBuilder()
               .UseKestrel()
               .UseContentRoot(Directory.GetCurrentDirectory())
               .ConfigureAppConfiguration((hostingContext, config) =>
               {
                   config
                       .SetBasePath(hostingContext.HostingEnvironment.ContentRootPath)
                       .AddJsonFile("appsettings.json", true, true)
                       .AddJsonFile($"appsettings.{hostingContext.HostingEnvironment.EnvironmentName}.json", true, true)
                       .AddJsonFile("ocelot.json")
                       .AddEnvironmentVariables();
               })
               .ConfigureServices(s => {
                   s.AddCors(o => o.AddPolicy("AllowAllPolicy", builder =>
                   {
                       builder.AllowAnyOrigin()
                           .AllowAnyMethod()
                           .AllowAnyHeader()
                           .WithExposedHeaders("Authorization");
                   }));
                   
                   s.AddOcelot();
               })
            //    .ConfigureLogging((hostingContext, logging) =>
            //    {
            //        //add your logging
            //    })
               .UseIISIntegration()
               .Configure(async app =>
               {
                   await app
                       .UseCors("AllowAllPolicy")
                       .UseWebSockets()
                       .UseOcelot();
               })
               .Build()
               .Run();
           }
       }
   }